Install dependencies
--------------------
```
npm install
bower install
```

Start the server
----------------
Start MongoDB
```
mongod
```

Start the API server
```
npm start
```

The API will be listening on [http://localhost:3000](http://localhost:3000)