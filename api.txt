
------------------
GET /api/v1/notes - get all notes
request body: none
response body:
[
    {
        "_id": "abcdefghjkl"
        "title": "Note title",
        "content": "Note content",
    },
    ...
]

------------------
POST /api/v1/notes - create a note
request body:
{
    "title": "Note title",      // required
    "content": "Note content"   // required
}

------------------
GET /api/v1/notes/[note._id] - get specific note
request body: none
response body:
{
    "_id": "abcdefghjkl"
    "title": "Note title",
    "content": "Note content",
}

------------------
PUT /api/v1/notes/[note._id] - update a note
request body:
{
    "title": "New title",     // optional
    "content": "New content"  // optional
}

------------------
DELETE /api/v1/notes/[note._id] - delete a note
request body: none

------------------