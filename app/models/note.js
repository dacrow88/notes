/**
 * Created by boris on 29.10.2016, 00:04.
 */
const mongoose = require('mongoose');

const notes_schema = new mongoose.Schema({
    title: String,
    content: String,
    x: {type: Number, default: 0},
    y: {type: Number, default: 0}
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    }
});

module.exports = mongoose.model('Note', notes_schema);