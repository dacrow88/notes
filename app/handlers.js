const Note = require('./models/note');

module.exports = {
    getAllNotes,
    createNote,
    deleteAllNotes,
    getNote,
    updateNote,
    deleteNote
};

/////////////////////////////////////////////////

function getAllNotes(req, res) {
    Note.find().sort('-updated_at').exec((err, data) => {
        if (err) {
            console.error(err);
            res.status(500);
        }
        res.json(data);
    });
}

function createNote(req, res) {
    // both title and content are required
    if ( ! req.body.title || ! req.body.content) {
        res.status(400).send('Title and content are required!');
        return;
    }

    Note.create({
        title: req.body.title,
        content: req.body.content,
    }, (err, note) => {
        if (err) {
            console.error(err);
            res.status(500);
        } else {
            res.status(201);
        }
        res.json(note);
    });
}

function deleteAllNotes(req, res) {
    console.warn("WARNING: deleting all notes, this shouldn't usually be allowed!");
    Note.remove({}, () => res.send());
}

function getNote(req, res) {
    Note.findById(req.params.id, (err, data) => {
        if (err) console.error(err);
        if ( ! data) res.status(404);
        res.json(data);
    });
}

function updateNote(req, res) {
    // either title or content is required
    if ( ! req.body.title && ! req.body.content) {
        res.status(400).send('Title or content is required!');
        return;
    }

    let data = {};
    if (typeof req.body.title !== 'undefined') {
        data.title = req.body.title;
    }
    if (typeof req.body.content !== 'undefined') {
        data.content = req.body.content;
    }
    if (typeof req.body.x !== 'undefined') {
        data.x = req.body.x;
    }
    if (typeof req.body.y !== 'undefined') {
        data.y = req.body.y;
    }

    Note.findByIdAndUpdate({
        _id: req.params.id
    }, data, {new: true}, (err, note) => {
        if (err) {
            console.error(err);
            res.status(500);
        } else if ( ! note) {
            res.status(404);
        }
        res.json(note);
    })
}

function deleteNote(req, res) {
    Note.findByIdAndRemove(req.params.id, (err, note) => {
        if (err) {
            console.error(err);
            res.status(500);
        } else if ( ! note) {
            res.status(404);
        } else {
            res.json(note);
        }
    });
}