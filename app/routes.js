"use strict";

const express = require('express');
const router = express.Router();
const handlers = require('./handlers');

router.route('/api/v1/notes')
    .get(handlers.getAllNotes)
    .post(handlers.createNote)
    // .delete(handlers.deleteAllNotes)
;
router.route('/api/v1/notes/:id')
    .get(handlers.getNote)
    .put(handlers.updateNote)
    .delete(handlers.deleteNote);

module.exports = router;