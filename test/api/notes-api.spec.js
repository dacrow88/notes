/**
 * Created by boris on 30.10.2016, 02:06.
 */
"use strict";

// set node environment to 'test'
process.env.NODE_ENV = 'test';

// test utils
const chai = require('chai');
const should = chai.should();
const request = require('supertest');
const _ = require('lodash');

// app modules
const app = require('../../app');
const Note = require('../../app/models/note');


describe('API endpoint /api/v1/notes', function() {
    before('remove notes from previous tests', function(done) {
        Note.remove(done);
    });

    afterEach('remove created notes', function(done) {
        Note.remove(done);
    });

    it('should create a note', function(done) {
        const note = {
            title: 'test title',
            content: 'test content'
        };

        request(app)
            .post('/api/v1/notes')
            .send(note)
            .expect(201)
            .end(function(err, res) {
                if (err) throw err;
                res.body.should.have.property('_id');
                res.body.should.have.property('created_at');
                res.body.should.have.property('updated_at');
                res.body.title.should.equal(note.title);
                res.body.content.should.equal(note.content);

                // db should have the new record
                Note.findById(res.body._id).lean().exec(function(err, data) {
                    data.title.should.equal(note.title);
                    data.content.should.equal(note.content);
                    done();
                });
            });
    });

    describe('Create test notes and act on them', function() {
        let notes = [];

        beforeEach('create test notes directly in db', function(done) {
            Note.create([1,2,3].map(x => ({
                title: `note-${x}`,
                content: `note-${x} content`
            })), function(err, entries) {
                if (err) return done(err);
                notes = JSON.parse(JSON.stringify(entries));
                done();
            });
        });

        it('should list all notes', function(done) {
            request(app)
                .get('/api/v1/notes')
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;

                    res.body.should.be.an('array');
                    res.body.should.have.length(3);
                    done();
                });
        });

        it('should list a single note', function(done) {
            const note = notes[0];
            request(app)
                .get(`/api/v1/notes/${note._id}`)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;

                    res.body._id.should.equal(note._id);
                    res.body.title.should.equal(note.title);
                    res.body.content.should.equal(note.content);
                    res.body.x.should.be.defined;
                    res.body.y.should.be.defined;
                    done();
                });
        });

        it('should update a note', function(done) {
            const old_note = notes[0];
            const data = {
                title: 'new note title ' + Math.random(),
                content: 'new note content ' + Math.random(),
                x: 100,
                y: 200
            };
            const expected_note = _.extend({}, old_note, data);

            request(app)
                .put(`/api/v1/notes/${old_note._id}`)
                .send(data)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;

                    res.body.title.should.equal(expected_note.title);
                    res.body.content.should.equal(expected_note.content);
                    res.body.x.should.equal(expected_note.x);
                    res.body.y.should.equal(expected_note.y);

                    // db should have the record updated
                    Note.findById(old_note._id).lean().exec(function(err, note) {
                        if (err) throw err;
                        note.title.should.equal(expected_note.title);
                        note.content.should.equal(expected_note.content);
                        note.x.should.equal(expected_note.x);
                        note.y.should.equal(expected_note.y);
                        done();
                    });
                });
        });

        it('should delete a note', function(done) {
            const note = notes[0];

            request(app)
                .delete(`/api/v1/notes/${note._id}`)
                .expect(200)
                .end(function(err, res) {
                    if (err) throw err;

                    // server should return the record it just deleted
                    res.body._id.should.equal(note._id);

                    // db shouldn't have the deleted record
                    Note.findById(note._id).lean().exec(function(err, doc) {
                        if (err) throw err;
                        should.not.exist(doc);
                        done();
                    });
                })
        });
    });
});