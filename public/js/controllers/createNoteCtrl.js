app.controller('createNoteCtrl', ['$scope','$http','$timeout', function($scope, $http, $timeout) {
    $scope.new_note = {};

    $scope.createNote = function() {
        $http.post('/api/v1/notes', $scope.new_note, $timeout)
            .then(function(res) {
                $scope.msg = 'The note is successfully submitted.';
                $scope.$close(true);
                $timeout(function() {
                    $scope.msg = '';
                },3000);
                delete $scope.err;
                $scope.new_note = {};
            }, function(res) {
                $scope.err = 'The server returned an error.';
            });
    }
}]);