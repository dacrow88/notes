app.controller('editNoteCtrl', ['$scope','$http', 'note', function($scope, $http, note) {
    $scope.note = angular.copy(note);

    $scope.editNote = function() {
        $http.put('/api/v1/notes/' + $scope.note._id, $scope.note)
            .then(function(res) {
                $scope.$close(true);
            })
    }
}]);