app.controller('appController', ['$scope', '$http', '$timeout', '$uibModal', function ($scope, $http, $timeout, $uibModal) {
    $scope.notes = [{
        title: 'test',
        content: 'test',
        _id: '123'
    }];

    $scope.refreshNotes = function () {
        $http.get('/api/v1/notes')
            .then(function (res) {
                $scope.notes = res.data;
                delete $scope.err;
            }, function (res) {
                $scope.err = 'The server returned an error.';
            });
    };

    $scope.refreshNotes();

    $scope.createNote = function () {
        $uibModal.open({
            controller: 'createNoteCtrl',
            templateUrl: 'views/createNote.html',
            backdrop: 'static'
        }).result.then($scope.refreshNotes);
    };

    $scope.removeNote = function(note) {
        $http.delete('/api/v1/notes/' + note._id)
            .then(function(res) {
                $scope.refreshNotes();
            }, function(res) {
                $scope.err = 'The server returned an error.';
            });
    };

    $scope.editNote = function(note) {
        $uibModal.open({
            controller: 'editNoteCtrl',
            templateUrl: 'views/editNote.html',
            resolve: {
                note: function() {
                    return note;
                }
            },
            backdrop: 'static'
        }).result.then($scope.refreshNotes);
    };

}]);
