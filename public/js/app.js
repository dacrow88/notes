/**
 * Created by Aleks on 29-Oct-16.
 */
var app = angular.module('app', [
    'ui.bootstrap',
    'ngSanitize',
    'froala'
]);

app.value('froalaConfig', {
    toolbarButtons: ["bold", "italic", "underline", "|", "align", "formatOL", "formatUL", 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize','quote', 'clearFormatting', 'undo', 'redo'],
    heightMin: 150,
    width: 580
});

